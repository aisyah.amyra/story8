from django.test import TestCase
from django.test import Client
from django.urls import reverse, resolve
from .views import books;

class UnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.books = reverse('books')


    def test_url_exist(self):
        response2 = Client().get(self.books)
        self.assertEqual(response2.status_code, 200)

    def test_url_not_exist(self):
        response = Client().get('/oi')
        self.assertEqual(response.status_code, 404)

    def test_page(self):
        response2 = Client().get(self.books)
        self.assertTemplateUsed(response2, 'webbook.html')
